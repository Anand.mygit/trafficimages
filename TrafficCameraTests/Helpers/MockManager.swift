//
//  MockManager.swift
//  TrafficCameraTests
//
//  Created by Chetan Anand on 1/9/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation

class MockManager {
    private static func getJsonDataFromFile(filename: String?) -> Data? {
        let bundle = Bundle(for: MockManager.self)
        guard let path = bundle.path(forResource: filename, ofType: "json") else {
            return nil
        }
        let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        return data
    }

    static func getjsonObject<T: Codable>(_: T.Type, filename: String?) -> T? {
        guard let data = getJsonDataFromFile(filename: filename) else {
            return nil
        }
        let jsonObject = try? JSONDecoder().decode(T.self, from: data)
        return jsonObject
    }
}
