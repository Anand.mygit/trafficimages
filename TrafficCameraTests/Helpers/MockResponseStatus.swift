//
//  MockResponseStatus.swift
//  TrafficCameraTests
//
//  Created by Chetan Anand on 1/9/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
enum MockResponseStatus: String {
    case success, failure
}
