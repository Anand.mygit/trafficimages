//
//  TrafficCameraDetailTests.swift
//  TrafficCameraTests
//
//  Created by Chetan Anand on 1/9/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

@testable import TrafficCamera
import XCTest

class TrafficCameraDetailTests: XCTestCase {
    let viewController = MapDetailViewController()
    let interactor = MapDetailInteractor()
    let presenter = MapDetailPresenter()
    let workerMock = MapDetailWorkerMock()
    let mapDetailViewControllerSpy = MapDetailViewControllerSpy()

    // MARK: Setup

    override func setUpWithError() throws {
        viewController.output = interactor
        interactor.output = presenter
        interactor.worker = workerMock
        presenter.output = mapDetailViewControllerSpy
    }

    override func tearDownWithError() throws {}

    // MARK: Test cases

    func test_fetchData_whenSuccess() throws {
        workerMock.mockJsonfilename = MockResponseStatus.success.rawValue
        guard let mapResponse = MockManager.getjsonObject(MapResponse.self, filename: "MapSuccess") else {
            return
        }
        let mapCoordinate = MapCoordinate(latitude: 10.0, longitude: 20.0)
        viewController.output?.getMapData(response: mapResponse, mapCoordinate: mapCoordinate)
        XCTAssertTrue(mapDetailViewControllerSpy.isGetMapDataCalled)
    }

    func test_getImage_whenSuccess() throws {
        workerMock.mockJsonfilename = MockResponseStatus.success.rawValue
        viewController.output?.getImage(url: "")
        XCTAssertTrue(mapDetailViewControllerSpy.isSetImageCalled)
    }

    func test_getImage_whenFailure() throws {
        workerMock.mockJsonfilename = MockResponseStatus.failure.rawValue
        viewController.output?.getImage(url: "")
        XCTAssertFalse(mapDetailViewControllerSpy.isSetImageCalled)
    }
}

class MapDetailViewControllerSpy: MapDetailPresenterOutput {
    var isGetMapDataCalled = false
    var isSetImageCalled = false
    func setMapDetails(mapData _: MapData?) {
        isGetMapDataCalled = true
    }

    func setImage(image _: UIImage?) {
        isSetImageCalled = true
    }
}
