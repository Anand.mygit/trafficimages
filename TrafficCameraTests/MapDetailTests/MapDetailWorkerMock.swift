//
//  MapDetailWorkerMock.swift
//  TrafficCameraTests
//
//  Created by Chetan Anand on 1/9/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
@testable import TrafficCamera
import UIKit

class MapDetailWorkerMock: MapDetailWorkerProtocol {
    var mockJsonfilename: String?
    func requestMapImage(request _: MapDetailRequest?, success: @escaping (MapDetailResponse) -> Void, failure: @escaping ((NSError?) -> Void)) {
        if mockJsonfilename == MockResponseStatus.success.rawValue {
            success(MapDetailResponse(image: UIImage()))
        } else {
            failure(nil)
        }
    }
}
