//
//  MapMockWorker.swift
//  TrafficCameraTests
//
//  Created by Chetan Anand on 1/9/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
@testable import TrafficCamera

class MapWorkerMock: MapWorkerProtocol {
    var mockJsonfilename: String?
    func requestMapData(request _: MapRequest?, success: @escaping (MapResponse) -> Void, failure: @escaping ((NSError?) -> Void)) {
        guard let mapResponse = MockManager.getjsonObject(MapResponse.self, filename: mockJsonfilename) else {
            let error = NSError(domain: "", code: 0, userInfo: nil)
            failure(error)
            return
        }
        guard mapResponse.items != nil else {
            let error = NSError(domain: "", code: 0, userInfo: nil)
            failure(error)
            return
        }
        success(mapResponse)
    }
}
