//
//  TrafficCameraTests.swift
//  TrafficCameraTests
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import MapKit
@testable import TrafficCamera
import XCTest

class MapViewControllerTests: XCTestCase {
    let viewController = MapViewController()
    let interactor = MapInteractor()
    let presenter = MapPresenter()
    let workerMock = MapWorkerMock()
    let mapViewControllerSpy = MapViewControllerSpy()

    // MARK: Setup

    override func setUpWithError() throws {
        viewController.output = interactor
        interactor.output = presenter
        interactor.worker = workerMock
        presenter.output = mapViewControllerSpy
    }

    override func tearDownWithError() throws {}

    // MARK: Test cases

    func test_fetchData_whenSuccess() throws {
        workerMock.mockJsonfilename = "MapSuccess" // Stub JSON
        viewController.output?.fetchData()
        XCTAssertTrue(mapViewControllerSpy.isEnquirySuccessDataCalled)
    }

    func test_fetchData_whenFailure() throws {
        workerMock.mockJsonfilename = "MapFailure" // Stub JSON
        viewController.output?.fetchData()
        XCTAssertTrue(mapViewControllerSpy.isEnquiryFailureDataCalled)
    }
}

class MapViewControllerSpy: MapPresenterOutput {
    var isEnquirySuccessDataCalled = false
    var isEnquiryFailureDataCalled = false

    func enquirySuccessData(annotations _: [MKPointAnnotation], response _: MapResponse) {
        isEnquirySuccessDataCalled = true
    }

    func enquiryFailureData() {
        isEnquiryFailureDataCalled = true
    }
}
