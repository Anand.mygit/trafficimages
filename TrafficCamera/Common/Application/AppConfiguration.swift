//
//  AppConfiguration.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
final class AppConfiguration {
    /// Set the base URL in Build Settings
    lazy var apiBaseURL: String = {
        guard let apiBaseURL = Bundle.main.object(forInfoDictionaryKey: "ApiBaseURL") as? String else {
            fatalError("ApiBaseURL must not be empty in plist")
        }
        return apiBaseURL
    }()
}
