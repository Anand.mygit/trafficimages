//
//  AppDelegate.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
