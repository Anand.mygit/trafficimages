//
//  MapResource.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 1/9/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
struct MapResource {
    struct Title {
        static let trafficCameraText = "Traffic Cameras"
        static let trafficImageText = "Traffic Image"
    }

    struct MapDetail {
        static let cameraIdText = "Camera ID:"
        static let latitudeText = "Latitude:"
        static let longitudeText = "Longitude:"
    }
}
