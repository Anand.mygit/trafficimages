//
//  MapInteractor.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
protocol MapDetailInteractorInput {
    func getMapData(response: MapResponse?, mapCoordinate: MapCoordinate?)
    func getImage(url: String?)
}

protocol MapDetailInteractorOutput {
    func getMapData(response: MapResponse?, mapCoordinate: MapCoordinate?)
    func imageDataSuccess(response: MapDetailResponse?)
    func imageDataFailure()
}

class MapDetailInteractor {
    var output: MapDetailInteractorOutput?
    var worker: MapDetailWorkerProtocol? = MapDetailWorker()
}

extension MapDetailInteractor: MapDetailInteractorInput {
    func getMapData(response: MapResponse?, mapCoordinate: MapCoordinate?) {
        output?.getMapData(response: response, mapCoordinate: mapCoordinate)
    }

    func getImage(url: String?) {
        let request = MapDetailRequest(imageUrl: url)
        worker?.requestMapImage(request: request, success: { [weak self] response in
            self?.output?.imageDataSuccess(response: response)
        }, failure: { [weak self] _ in
            self?.output?.imageDataFailure()
        })
    }
}
