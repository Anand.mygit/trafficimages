//
//  MapDetailPresenter.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
import UIKit
protocol MapDetailPresenterInput {
    func getMapData(response: MapResponse?, mapCoordinate: MapCoordinate?)
    func imageDataSuccess(response: MapDetailResponse?)
    func imageDataFailure()
}

protocol MapDetailPresenterOutput {
    func setMapDetails(mapData: MapData?)
    func setImage(image: UIImage?)
}

class MapDetailPresenter {
    var output: MapDetailPresenterOutput?
}

extension MapDetailPresenter: MapDetailPresenterInput {
    func getMapData(response: MapResponse?, mapCoordinate: MapCoordinate?) {
        let camera = response?.items?.first?.cameras?.filter { $0.location?.latitude == mapCoordinate?.latitude && $0.location?.longitude == mapCoordinate?.longitude }.first
        let cameraId = "\(MapResource.MapDetail.cameraIdText) \(camera?.camera_id ?? "")"
        let latitude = "\(MapResource.MapDetail.latitudeText) \(camera?.location?.latitude ?? 0.0)"
        let longitude = "\(MapResource.MapDetail.longitudeText) \(camera?.location?.longitude ?? 0.0)"
        var mapData = MapData()
        mapData.cameraId = camera?.camera_id
        mapData.imageUrl = camera?.image
        mapData.latitude = latitude
        mapData.longitude = longitude
        mapData.cameraId = cameraId
        output?.setMapDetails(mapData: mapData)
    }

    func imageDataSuccess(response: MapDetailResponse?) {
        output?.setImage(image: response?.image)
    }

    func imageDataFailure() {}
}
