//
//  MapConfigurator.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation

// MARK: - Output Protocols

extension MapDetailViewController: MapDetailPresenterOutput {}
extension MapDetailInteractor: MapDetailViewControllerOutput {}
extension MapDetailPresenter: MapDetailInteractorOutput {}

// MARK: - MapDetailConfigurator

class MapDetailConfigurator {
    static func configure(_ viewController: MapDetailViewController) {
        guard viewController.output == nil else {
            return
        }
        let router = MapDetailRouter()
        let presenter = MapDetailPresenter()
        let interactor = MapDetailInteractor()
        router.viewController = viewController
        viewController.router = router
        viewController.output = interactor
        interactor.output = presenter
        presenter.output = viewController
    }
}
