//
//  MapViewController.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import UIKit
protocol MapDetailViewControllerInput {
    func setMapDetails(mapData: MapData?)
    func setImage(image: UIImage?)
}

protocol MapDetailViewControllerOutput {
    func getMapData(response: MapResponse?, mapCoordinate: MapCoordinate?)
    func getImage(url: String?)
}

// MARK: MapDetailViewController

final class MapDetailViewController: UIViewController {
    @IBOutlet var cameraIdlabel: UILabel!
    @IBOutlet var latitidelabel: UILabel!
    @IBOutlet var longitudelabel: UILabel!

    @IBOutlet var imageView: UIImageView!
    var router: MapDetailRouter?
    var output: MapDetailViewControllerOutput?
    var mapresponse: MapResponse?
    var mapCoordinate: MapCoordinate?

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.enableZoom()
        title = MapResource.Title.trafficImageText
        MapDetailConfigurator.configure(self)
        output?.getMapData(response: mapresponse, mapCoordinate: mapCoordinate)
    }
}

// MARK: MapDetailViewControllerInput

extension MapDetailViewController: MapDetailViewControllerInput {
    func setMapDetails(mapData: MapData?) {
        cameraIdlabel.text = mapData?.cameraId
        latitidelabel.text = mapData?.latitude
        longitudelabel.text = mapData?.longitude
        showLoading()
        output?.getImage(url: mapData?.imageUrl)
    }

    func setImage(image: UIImage?) {
        hideLoading()
        imageView.image = image
    }
}
