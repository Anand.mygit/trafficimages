//
//  MapRouter.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
import MapKit
class MapRouter {
    weak var viewController: MapViewController?
    func goToDetail(response: MapResponse?, mapCoordinate: MapCoordinate?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let detailViewController = storyboard.instantiateViewController(withIdentifier: "MapDetailViewController") as? MapDetailViewController else {
            return
        }
        detailViewController.mapresponse = response
        detailViewController.mapCoordinate = mapCoordinate
        viewController?.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
