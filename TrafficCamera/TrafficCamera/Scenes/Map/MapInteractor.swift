//
//  MapInteractor.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
protocol MapInteractorInput {
    func fetchData()
}

protocol MapInteractorOutput {
    func enquirySuccessData(response: MapResponse)
    func enquiryFailureData()
}

class MapInteractor {
    var output: MapInteractorOutput?
    var worker: MapWorkerProtocol? = MapWorker()
}

extension MapInteractor: MapInteractorInput {
    func fetchData() {
        let request = MapRequest(dateTime: nil)
        worker?.requestMapData(request: request, success: { [weak self] response in
            self?.output?.enquirySuccessData(response: response)
        }, failure: { [weak self] _ in
            self?.output?.enquiryFailureData()
        })
    }
}
