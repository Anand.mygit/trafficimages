//
//  MapConfigurator.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation

// MARK: - Output Protocols

extension MapViewController: MapPresenterOutput {}
extension MapInteractor: MapViewControllerOutput {}
extension MapPresenter: MapInteractorOutput {}

// MARK: - MapConfigurator

class MapConfigurator {
    static func configure(_ viewController: MapViewController) {
        guard viewController.output == nil else {
            return
        }
        let router = MapRouter()
        let presenter = MapPresenter()
        let interactor = MapInteractor()
        router.viewController = viewController
        viewController.router = router
        viewController.output = interactor
        interactor.output = presenter
        presenter.output = viewController
    }
}
