//
//  MapPresenter.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
import MapKit
protocol MapPresenterInput {
    func enquirySuccessData(response: MapResponse)
    func enquiryFailureData()
}

protocol MapPresenterOutput {
    func enquirySuccessData(annotations: [MKPointAnnotation], response: MapResponse)
    func enquiryFailureData()
}

class MapPresenter {
    var output: MapPresenterOutput?
    fileprivate var annotations = [MKPointAnnotation]()
}

extension MapPresenter: MapPresenterInput {
    func enquirySuccessData(response: MapResponse) {
        guard let cameras = response.items?.first?.cameras else {
            return
        }
        for camera in cameras {
            if let latitude = camera.location?.latitude, let longitude = camera.location?.longitude {
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                annotations.append(annotation)
            }
        }
        output?.enquirySuccessData(annotations: annotations, response: response)
    }

    func enquiryFailureData() {
        output?.enquiryFailureData()
    }
}
