//
//  MapViewController.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import MapKit
import UIKit
protocol MapViewControllerInput {
    func enquirySuccessData(annotations: [MKPointAnnotation], response: MapResponse)
}

protocol MapViewControllerOutput {
    func fetchData()
}

// MARK: MapViewController

final class MapViewController: UIViewController {
    var router: MapRouter?
    var output: MapViewControllerOutput?
    var mapresponse: MapResponse?
    @IBOutlet var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        title = MapResource.Title.trafficImageText
        MapConfigurator.configure(self)
        output?.fetchData()
    }
}

// MARK: MapViewControllerInput

extension MapViewController: MapViewControllerInput {
    func enquirySuccessData(annotations: [MKPointAnnotation], response: MapResponse) {
        mapresponse = response
        mapView.addAnnotations(annotations)
    }

    func enquiryFailureData() {
        let allAnnotations = mapView.annotations
        mapView.removeAnnotations(allAnnotations)
    }
}

// MARK: MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        var mapCoordinate = MapCoordinate()
        mapCoordinate.longitude = view.annotation?.coordinate.longitude
        mapCoordinate.latitude = view.annotation?.coordinate.latitude
        router?.goToDetail(response: mapresponse, mapCoordinate: mapCoordinate)
        mapView.deselectAnnotation(view.annotation, animated: true)
    }
}
