//
//  MapEndpoint.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Alamofire
import Foundation
enum MapEndpoint: URLRequestConvertible {
    case trafficImages(dateTime: String?)

    enum Constants {
        static let baseURLPath = AppConfiguration().apiBaseURL
    }

    var method: HTTPMethod {
        switch self {
        case .trafficImages:
            return .get
        }
    }

    var path: String {
        return "/transport/traffic-images"
    }

    var parameters: [String: Any] {
        switch self {
        case let .trafficImages(dateTime):
            guard let dateTime = dateTime else {
                return [:]
            }
            return ["date_time": dateTime]
        }
    }

    public func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseURLPath.asURL()
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue
        request.timeoutInterval = TimeInterval(10 * 1000)
        return try URLEncoding.default.encode(request, with: nil)
    }
}
