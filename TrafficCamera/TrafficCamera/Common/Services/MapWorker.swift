//
//  MapWorker.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Alamofire
import Foundation
protocol MapWorkerProtocol {
    var mockJsonfilename: String? { get set }
    func requestMapData(request: MapRequest?, success: @escaping (MapResponse) -> Void, failure: @escaping ((NSError?) -> Void))
}

class MapWorker: MapWorkerProtocol {
    var mockJsonfilename: String?
    func requestMapData(request: MapRequest?, success: @escaping (MapResponse) -> Void, failure: @escaping ((NSError?) -> Void)) {
        AF.request(MapEndpoint.trafficImages(dateTime: request?.dateTime)).responseDecodable(of: MapResponse.self) { response in
            switch response.result {
            case let .success(value):
                success(value)
            case let .failure(error):
                failure(error as NSError)
            }
        }
    }
}
