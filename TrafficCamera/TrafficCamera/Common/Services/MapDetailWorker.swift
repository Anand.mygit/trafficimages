//
//  MapDetailWorker.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Alamofire
import Foundation
protocol MapDetailWorkerProtocol {
    var mockJsonfilename: String? { get set }
    func requestMapImage(request: MapDetailRequest?, success: @escaping (MapDetailResponse) -> Void, failure: @escaping ((NSError?) -> Void))
}

class MapDetailWorker: MapDetailWorkerProtocol {
    var mockJsonfilename: String?
    func requestMapImage(request: MapDetailRequest?, success: @escaping (MapDetailResponse) -> Void, failure _: @escaping ((NSError?) -> Void)) {
        guard let imageUrl = request?.imageUrl, let url = URL(string: imageUrl) else {
            return
        }
        AF.download(url).responseData { response in
            if let data = response.value {
                let image = UIImage(data: data)
                let mapDetailResponse = MapDetailResponse(image: image)
                success(mapDetailResponse)
            }
        }
    }
}
