//
//  MapResponse.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//
import Foundation
struct MapResponse: Codable {
    let items: [Items]?
    let api_info: ApiInfo?

    enum CodingKeys: String, CodingKey {
        case items
        case api_info
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([Items].self, forKey: .items)
        api_info = try values.decodeIfPresent(ApiInfo.self, forKey: .api_info)
    }
}
