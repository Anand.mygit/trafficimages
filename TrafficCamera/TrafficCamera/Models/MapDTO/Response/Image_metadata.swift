//
//  Image_metadata.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//
import Foundation
struct Image_metadata: Codable {
    let height: Int?
    let width: Int?
    let md5: String?

    enum CodingKeys: String, CodingKey {
        case height
        case width
        case md5
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        height = try values.decodeIfPresent(Int.self, forKey: .height)
        width = try values.decodeIfPresent(Int.self, forKey: .width)
        md5 = try values.decodeIfPresent(String.self, forKey: .md5)
    }
}
