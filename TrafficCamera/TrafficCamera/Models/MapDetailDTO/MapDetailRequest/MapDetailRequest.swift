//
//  MapDetailRequest.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
struct MapDetailRequest: Codable {
    let imageUrl: String?
}
