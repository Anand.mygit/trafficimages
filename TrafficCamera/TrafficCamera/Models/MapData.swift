//
//  MapData.swift
//  TrafficCamera
//
//  Created by Chetan Anand on 31/8/20.
//  Copyright © 2020 Chetan Anand. All rights reserved.
//

import Foundation
struct MapData {
    var imageUrl: String?
    var cameraId: String?
    var latitude: String?
    var longitude: String?
}

struct MapCoordinate {
    var latitude: Double?
    var longitude: Double?
}
